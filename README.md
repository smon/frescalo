# Frescalo

Native R rewrite of Hill (2012; DOI:10.1111/j.2041-210X.2011.00146.x), which was written in Fortran code, available at https://www.brc.ac.uk/biblio/frescalo-computer-program-analyse-your-biological-records

R code adapted from Dr. Jon Yearsley https://github.com/DrJonYearsley/frescaloR
(and Oli Pescott https://github.com/sacrevert/fRescalo)


## Frescalo Analyse Baden-Württemberg

Analyse der floristischen Daten aus Baden-Württemberg (Arno Wörz und Mike Thiv, Staatliches Museum für Naturkunde in Stuttgart)

https://naturkundemuseum-bw.de/forschung/botanik
In Baden-Württemberg gibt es über 2100 wild wachsende Pflanzenarten. Mehr als ein Drittel (36,2 %) sind gefährdet und stehen auf der Roten Liste.

Die Naturkundemuseen in Stuttgart und Karlsruhe führten mit 250 ehrenamtlichen Mitarbeitern zwischen 1970 und 1998 eine floristische Kartierung zur vollständigen Erfassung der Gefäßpflanzen Baden-Württembergs durch. Die Ergebnisse sind in dem achtbändigen Werk "Die Farn- und Blütenpflanzen Baden-Württembergs" zusammengefasst mit Verbreitungskarten, Artbeschreibungen, Angaben zur Verbreitung, Ökologie und Gefährdung sowie einem Foto. Diese Bestandsaufnahme diente als Grundlage für die Rote Liste Baden-Württembergs und für das Artenschutzprogramm der Landesanstalt für Umweltschutz. Sie ist auch die Basis für unser aktuelles Projekt, der Beteiligung am F+E-Vorhaben „Pflanzenverbreitung im Klimawandel“ des BfN, bei dem u.a. ein bundesweiter Atlas der Farn- und Blütenpflanzen erstellt wurde.

Die Flora unterliegt ständigen Veränderungen: Neue Arten wandern ein, andere gehen zurück oder sterben ganz aus. Deshalb läuft seit 2008 eine komplette Neukartierung des ganzen Landes in Kooperation mit der BAS und AHO und unter Beteiligung von 150 ehrenamtlichen Mitarbeitern. Inzwischen liegen über 2,5 Millionen Datensätze vor. Der aktuelle Stand wird online veröffentlicht unter http://www.flora.naturkundemuseum-bw.de. Geplant ist ein aktualisierter Kartenband, der den heutigen Stand der Flora von Baden-Württemberg und deren Veränderung seit der Beendigung der vorherigen Kartierung (1998) dokumentiert.

http://www.flora.naturkundemuseum-bw.de./verbreitungskarten.htm

15.01.2016 16:24
Veränderungen in der Pflanzenwelt von Baden-Württemberg.
Meike Rech Presse
Staatliches Museum für Naturkunde Stuttgart
Unter der Federführung des Staatlichen Museums für Naturkunde Stuttgart erfassen mehr als 180 Ehrenamtliche zum zweiten Mal alle Pflanzen in Baden-Württemberg. Forscher des Naturkundemuseums können so die Auswirkungen des Klimawandels und des Menschen auf die regionale Pflanzenwelt erforschen. Zahlreiche gefährdete Arten nehmen weiterhin ab. Neue, wärmeliebende Pflanzen breiten sich aus.

Stuttgart. Eine erste Erfassung aller Pflanzenarten Baden-Württembergs erfolgte bereits von 1970 bis in die 1990er Jahre. Derzeit läuft eine neue Phase der sogenannten Floristischen Kartierung des Landes. Das lässt vergleichende Aussagen über die sichtbaren Änderungen der Pflanzenwelt in Baden-Württemberg in den letzten Jahrzehnten zu. Die Botaniker Dr. Arno Wörz und Dr. Mike Thiv vom Staatlichen Museum für Naturkunde in Stuttgart stellen in einer vergleichenden Studie eine Reihe von Tendenzen und Veränderungen fest.

Viele heimische Arten, die schon seit langem auf der Roten Liste gefährdeter Pflanzenarten stehen, nehmen weiterhin kontinuierlich ab. Beispiele hierfür sind Frauenschuh, Arnika, Bärlappe, Lungenenzian oder Fleischfarbenes Knabenkraut. Gleichzeitig nehmen neue Arten, die aus anderen Ländern stammen, kontinuierlich zu. Diese sogenannten Neophyten wachsen meist an stark vom Menschen beeinflussten Stellen wie in Städten, an Parkplätzen, auf Bahnschotterflächen oder Straßenrändern. Eine starke Ausbreitung ist beispielsweise für das aus Südafrika stammende Schmalblättrige Greiskraut oder das Indische Springkraut erkennbar. Die Forscher stellten außerdem fest, dass wärmeliebende heimische Arten, wie die Riemenzunge, deutlich zunehmen. Arten, die kühlere Standorte bevorzugen, z.B. der Jura-Streifenfarn oder Flachbärlappe, nehmen hingegen ab. Auch nährstoff- und vor allem stickstoffliebende Arten nehmen zu, während die Arten nährstoffarmer Stellen, von denen viele auf der Roten Liste stehen, ebenfalls abnehmen.

Wodurch werden diese Veränderungen der Pflanzenwelt verursacht? Der Klimawandel spielt hier eine wichtige Rolle. Aber gibt es auch noch weitere Faktoren? Genauso wichtig ist die übermäßige Nährstoffzufuhr durch Düngung, überwiegend aus der Landwirtschaft und die zunehmende Urbanisierung in Baden-Württemberg, die vor allem die Ausbreitung neuer Arten begünstigt und seltene zurückdrängt.

„Naturschutzmaßnahmen sollten sich neben dem Klimaschutz also auch auf den Flächenverbrauch und Nährstoffeinträge konzentrieren. Sonst bleibt zwischen Überdüngung, Bautätigkeit und Klimawandel für seltene heimische Pflanzen nur noch wenig Platz“, so der Botaniker Dr. Mike Thiv.

Die Erfassung aller Pflanzen und deren Standorte in ganz Baden-Württemberg ist ein Projekt, das durch die Mitwirkung von mehr als 180 Ehrenamtlichen möglich ist. Unter der Federführung von Dr. Arno Wörz und in Kooperation mit der Botanischen Arbeitsgemeinschaft Südwestdeutschland wird die zweite Floristische Kartierung des Bundeslandes durchgeführt. In akribischer Arbeit werden - Rasterfeld für Rasterfeld - die Standorte von Pflanzen im ganzen Land vermerkt.

Das Projekt wird vom Ministerium für Ländlichen Raum und Verbraucherschutz Baden-Württemberg unterstützt. „Die erneute Erfassung der Pflanzen lässt einen zeitlichen Vergleich zu. Nur so können wir die Unterschiede auf regionaler Ebene feststellen. Die ersten Ergebnisse liegen nun vor. Die Daten werden die Grundlage für weitere Analysen sein“, so Dr. Arno Wörz. Ziel ist es, zu erkunden, welche Art wo vorkommt und Untersuchungen zu Verbreitungsmustern, Einwanderungen und Rückgängen von Pflanzenarten vorzunehmen. So können Empfehlungen für Naturschutzmaßnahmen ausgesprochen werden. Die Daten werden an Naturschutzbehörden (LUBW) weitergeleitet und stehen ferner für Forschungszwecke zur Verfügung.

Die Ergebnisse der Datenerhebung werden zudem in Floristischen Karten festgehalten (www.flora.naturkundemuseum-bw.de). Die Karten zeigen unter anderem die Ausbreitung von neuen Arten - wie beispielsweise dem Schmalblättrigen Greiskraut - entlang der Autobahnen, wie der A 81 und A5. Damit wird gezeigt, dass Veränderungen in der Flora in relativ kurzer Zeit möglich sind.

Die Untersuchung wurde in der Zeitschrift Flora veröffentlicht:

Wörz, A. & Thiv, M. The temporal dynamics of a regional flora—The effects of global and local impacts. Flora - Morphology, Distribution, Functional Ecology of Plants 217, 99-108 (2015).

