### Find time periods and species groups for frescalo analysis

# Import data
# orchi <- readRDS('./data/BW/orchidaceae.Rds')

right = function(text, num_char) {
  substr(text, nchar(text) - (num_char-1), nchar(text))
}
# 
# orchi$Year <- sapply(orchi$Aufnahme.Datum, function(x) right(x, 4))
# library(parsedate)
# orchi$Date <- parse_date(orchi$Aufnahme.Datum, default_tz = '')
# 
# orchi <- orchi[!is.na(orchi$Year), ]
# orchi <- orchi[orchi$Year > 1930, ]
# orchi <- orchi[orchi$Year != '78 -',]

orchi$p <- orchi$Year
y <- sort(unique(orchi$p))
B <- y[1]
P <- 1

repeat {
  i = 1
  repeat {
    orchi$p[orchi$p == y[i]] <- as.character(P)
    taxaByP <- length(unique(orchi$TaxonUsageID[orchi$p == as.character(P)]))
    if(taxaByP > 35 | i > 100) {
      break
    } else i = i + 1
  }
  if(!any(as.numeric(orchi$p) > P)) break
  # sort(unique(orchi$p))
  # print(y[i])
  B <- cbind(B, y[i])
  P <- P + 1
}
  
length(B)
'
[1,] "1931" "1944" "1964" "1968" "1969" "1971" "1974" "1979" "1981" "1983" "1985" "1987" "1990" "1993" "1994" "1996" "1998" "2000" "2002" "2004" "2005" "2007" "2008" "2009" "2012" "2018"
'

# # Add time period classification to data
# periods <- data.frame(t = c(1,2,3,4,5,6), 
#             start = as.POSIXct(c("1930-01-01", "1970-01-01", "1980-01-01", "1990-01-01", "2000-01-01", "2010-01-01"), "%Y-%m-%d"), 
#             end   = as.POSIXct(c("1969-12-31", "1979-12-31", "1989-12-31", "1999-12-31", "2009-12-31", "2019-12-31"), "%Y-%m-%d"))
# 
# suppressWarnings(
# orchi$period <- ifelse(orchi$Date >= periods$start[1] & orchi$Date <= periods$end[1], 1, 
#                 ifelse(orchi$Date >= periods$start[2] & orchi$Date <= periods$end[2], 2,
#                 ifelse(orchi$Date >= periods$start[3] & orchi$Date <= periods$end[3], 3, 
#                 ifelse(orchi$Date >= periods$start[4] & orchi$Date <= periods$end[4], 4, 
#                 ifelse(orchi$Date >= periods$start[5] & orchi$Date <= periods$end[5], 5, 
#                 ifelse(orchi$Date >= periods$start[6] & orchi$Date <= periods$end[6], 6, NA))))))
# )
# table(orchi$period)

# saveRDS(orchi, '../data/BW/orchidaceae.Rds')
