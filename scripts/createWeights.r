
require(reshape2)
createWeights <- function (distances, attributes, dist_sub = 200, sim_sub = 100, 
          normalise = FALSE, checks = TRUE, verbose = TRUE) {
  if(checks) errorChecks(dist = distances, sim = attributes, dist_sub = dist_sub, sim_sub = sim_sub)
  if(!is.character(distances[, 1])) distances[, 1] <- as.character(distances[, 1])
  if(!is.character(distances[, 2])) distances[, 2] <- as.character(distances[, 2])
  if(!is.character(attributes[, 1])) attributes[, 1] <- as.character(attributes[, 1])
  colnames(distances) <- c("site1", "site2", "distance")
  unique_dist_sites <- unique(c(distances[, 1], distances[, 2]))
  unique_sim_sites <- unique(attributes[, 1])
  distmiss <- unique_dist_sites[!unique_dist_sites %in% unique_sim_sites]
  simmiss <- unique_sim_sites[!unique_sim_sites %in% unique_dist_sites]
  missing <- unique(c(distmiss, simmiss))
  if (length(missing) > 0) {
    warning(paste("The following sites were in only one of 'attributes' and 'distances' and so have been excluded from the weights file:", toString(missing)))
    distances  <- distances[!distances[, 1] %in% missing, ]
    distances  <- distances[!distances[, 2] %in% missing, ]
    attributes <- attributes[!attributes[, 1] %in% missing, ]
  }
  if (normalise) {
    for (i in 2:length(colnames(attributes))) {
      mx <- max(attributes[, i])
      attributes[, i] <- attributes[, i]/mx
    }
  }
  if (verbose)    cat("Creating similarity distance table...")
  row.names(attributes) <- attributes[, 1]
  sim_distance <- dist(attributes[, 2:length(names(attributes))], 
                       diag = TRUE, upper = TRUE)
  sim_distance <- melt(as.matrix(sim_distance))
  sim_distance$value <- (sim_distance$value/max(sim_distance$value))
  sim_distance[, 1] <- as.character(sim_distance[, 1])
  sim_distance[, 2] <- as.character(sim_distance[, 2])
  colnames(sim_distance) <- c("site1", "site2", "similarity")
  if (verbose)   cat("Complete\n")
  if (verbose)   cat("Creating weights file...\n0%\n")
  total <- length(unique(distances[, 1]))
  pb <- txtProgressBar(min = 0, max = total, style = 3)
  weights_list <- mclapply2(unique(distances[, 1]), function(i) {
    sim_foc <- sim_distance[sim_distance$site1 == i, ]
    dist_foc <- distances[distances$site1 == i, ]
    dist_foc$rankdist <- rank(dist_foc$distance, ties.method = "first")
    dist_foc <- dist_foc[dist_foc$rankdist <= dist_sub, ]
    ranks <- merge(x = dist_foc, y = sim_foc, by = c("site1", "site2"), all.x = TRUE, all.y = FALSE)
    ranks$rankflor <- rank(ranks$similarity, ties.method = "first")
    ranks <- ranks[ranks$rankflor <= sim_sub, ]
    ranks$distsim <- (1 - (((ranks$rankdist - 1)^2)/(dist_sub)^2))^4
    ranks$florsim <- (1 - (((ranks$rankflor - 1)^2)/(sim_sub)^2))^4
    ranks$weight <- ranks$distsim * ranks$florsim
    if (verbose) 
      setTxtProgressBar(pb, grep(paste0("^", i, "$"), unique(distances[, 1])))
    return(data.frame(target = ranks$site1, neighbour = ranks$site2, 
                      weight = round(ranks$weight, 4)))
  })
  cat('rbind:')
  weights_master <- do.call(rbind, weights_list)
  close(pb)
  if (verbose)    cat("Complete\n")
  return(weights_master)
}
