# 1. check, harmonize and filter primary data
# 2. define time periods, see "timePeriods.r"
# 3. run frescalo and trend analysis

my_packages <- c("foreach", "doParallel", "ggplot2")                                    # Specify the packages needed
not_installed <- my_packages[!(my_packages %in% installed.packages()[ , "Package"])]    # Extract not installed packages
if(length(not_installed)) install.packages(not_installed)                               # Install not installed packages

require(foreach, quietly=T)
require(doParallel, quietly=T)
cl <- makeCluster(2)
registerDoParallel(cl, cores=NULL)
source('./scripts/frescalo_functions.R')
R_star = 0.2703
trend_analysis = TRUE
Phi = 0.74    # The standardisation for recorder effort
chunkSize = 5 # Number of hectads to pass to each compute node
missing.data <- 2   # How to deal with missing values
# trend_analysis = TRUE

 s <- Data[which(!is.na(Data$period)), c('TaxonName','MTBQ', 'period')]  # specify column for correct time period 
 names(s) <- c("species", "location", "time")
 wgts <- readRDS('./data/BW/similarity_weights_100_of_200-BW.rds')
 names(wgts) <- c('location1','location2','w') # ,'w1','w2','nsim','ndist')

##############################################################
##  
 spLocations = unique(s$location)                 # Create list of locations
 speciesNames = as.character(unique(s$species))   # Create list of unique species
# The variable species could be made numerical to make it more efficient

# For each region record presence/absence of each species (a_ij in Hill 2011)
 locationGroups = as.factor(rep(c(1:ceiling(length(spLocations)/chunkSize)), each=chunkSize))
 sSplit = split(s, locationGroups[match(s$location, spLocations)])  # Split species data up into grids

 idx = iter(sSplit)
 speciesList <- foreach(spList = idx, .inorder=T, .combine='c') %dopar% {
   speciesListFun(spList, speciesNames)
 }
# Add an additional species list where everything is absent 
 speciesList[[length(speciesList)+1]] = rep(0, times=length(speciesNames))
 spLocations = c(spLocations,'null_location')

#################################################################
# For each focal region calculate the sampling effort multipler
 dSub = wgts[,1:3]
 names(dSub) <- c('location1', 'location2', 'w')
 dSub$location1 <- as.character(dSub$location1)
 dSub$location2 <- as.character(dSub$location2)
 location1List = as.character(unique(dSub$location1))    # Create list of unique focal regions
 location1Groups = as.factor(rep(c(1:ceiling(length(location1List)/chunkSize)),each=chunkSize))

 dSplit = split(dSub, location1Groups[match(dSub$location1, location1List)])  # Split data up into focal regions

  idx2 = iter(dSplit)
 output <- foreach(focalData = idx2, .inorder=T, .combine='cfun', .multicombine=TRUE) %dopar% {
    frescalo(focalData, speciesList, spLocations, speciesNames, missing.data=missing.data)         # frescalo function from Jon Yearsley
 }

################################################################ #
# Trend analysis  ####
# Do the Frescalo trend analysis if there are more than 1 time bins (use same location groups as sSplit)
sSplit2 = split(s, as.factor(s$time))  # Split species data up into periods
idx3 = iter(sSplit2)
trend.out <- foreach(s_data=idx3, .inorder=T, .combine='rbind') %dopar% {
    trend(s_data, output$freq.out)
}

table(trend.out$time)
bw_fs <- list()
bw_fs$output <- output
bw_fs$trend <- trend.out

saveRDS(bw_fs, './data/BW/bw_fs-OuA.Rds')

# tail(trend.out)
# table(trend.out$time)
# table(trend.out$species)

################################################################ #
stopCluster(cl)
gc()

