# A script to calculate the alpha values for Frescalo 
# This has been checked against Frescalo output and it agrees
# Written by Jon Yearsley (jon.yearsley@ucd.ie) 2nd Sept 2016
# Modified:
# 6/9/2016 (JY): Data sent to cluster nodes in chunks (variable=chunkSize)
# 8/9/2016 (JY): Save species frequency file and include expected species richness in output
# 6/10/2016 (JY): Include validated trend analysis
# 13/11/2020 (FJ): Adapt for generic use, compare to Pescott and frescalo.exe

# rm(list=ls())  # Remove all variables from the memory
my_packages <- c("foreach", "doParallel", "ggplot2")                                    # Specify the packages needed
not_installed <- my_packages[!(my_packages %in% installed.packages()[ , "Package"])]    # Extract not installed packages
if(length(not_installed)) install.packages(not_installed)                               # Install not installed packages

require(foreach, quietly=T)
require(doParallel, quietly=T)
cl <- makeCluster(2)
registerDoParallel(cl, cores=NULL)

source('scripts/frescalo_functions.R')
R_star = 0.2703
trend_analysis = TRUE
Phi = 0.74    # The standardisation for recorder effort
chunkSize = 5 # Number of hectads to pass to each compute node

# Import data
 load(file = "data/unicorns.rda")
# Compare data and configuration from sparta package
# Add time period classification to data
 periods <- data.frame(t = c(1,2), start = as.Date(c("1980-01-01", "1990-01-01"), "%Y-%m-%d"), 
                      end = as.Date(c("1989-12-31", "1999-12-31"), "%Y-%m-%d"))
 unicorns$period <- ifelse(unicorns$TO_STARTDATE >= as.POSIXct(periods$start[1]) & unicorns$Date <= as.POSIXct(periods$end[1]), 1, ifelse (unicorns$TO_STARTDATE >= as.POSIXct(periods$start[2]) & unicorns$Date <= as.POSIXct(periods$end[2]), 2, NA))
## Only keep data from the those time periods used in the sparta example
 table(unicorns$period)
 s <- unicorns[which(!is.na(unicorns$period)), c(3,5,6)]; names(s) <- c("location", "species", "time")
 wgts <- read.delim(file = "data/GB_LC_Wts.txt", header = F, sep = "") # can be calculated with function createWeights from the sparta package
 d <- wgts; names(d) <- c('location1','location2','w','w1','w2','nsim','ndist')

##############################################################
spLocations = unique(s$location)
speciesNames = as.character(unique(s$species))   # Create list of unique species
# The variable species could be made numerical to make it more efficient

# For each region record presence/absence of each species (a_ij in Hill 2011)
locationGroups = as.factor(rep(c(1:ceiling(length(spLocations)/chunkSize)),each=chunkSize))
sSplit = split(s, locationGroups[match(s$location, spLocations)])  # Split species data up into hectads

idx = iter(sSplit)
speciesList <- foreach(spList = idx, .inorder=T, .combine='c') %dopar% {
  speciesListFun(spList, speciesNames)
}
# Add an additional species list where everything is absent 
speciesList[[length(speciesList)+1]] = rep(0, times=length(speciesNames))
spLocations = c(spLocations,'null_location')

#################################################################
# For each focal regional calculate the sampling effort multipler
dSub = d[,1:3]
location1List = as.character(unique(dSub$location1))    # Create list of unique focal regions
location1Groups = as.factor(rep(c(1:ceiling(length(location1List)/chunkSize)),each=chunkSize))

dSplit = split(dSub, location1Groups[match(dSub$location1, location1List)])  # Split data up into focal regions
idx2 = iter(dSplit)
output <- foreach(focalData = idx2, .inorder=T, .combine='cfun', .multicombine=TRUE) %dopar% {
  frescalo(focalData, speciesList, spLocations, speciesNames)
}

### debug ####
# frescalo(dSplit[[462]], speciesList, spLocations, speciesNames, missing.data = 2)
# data_in <- dSplit[[462]]

unicorns_fs <- list()
unicorns_fs$stat <- output$frescalo.out
unicorns_fs$freq <- output$freq.out

# 
################################################################ #
# Trend analysis  ####
if (trend_analysis) {
  # Do the Frescalo trend analysis if there are more than 1 year bins (use same location groups as sSplit)
  sSplit2 = split(s, as.factor(s$time))  # Split species data up into periods
  idx3 = iter(sSplit2)
  trend.out <- foreach(s_data=idx3, .inorder=T, .combine='rbind') %dopar% {
    trend(s_data, output$freq.out)
  }
}

unicorns_fs$trend <- trend.out
################################################################ #
stopCluster(cl)
gc()


##################### #
## Compare results ####
##################### #
load(file = "data/unicorn_TF.rda")  # sparta frescalo.exe results
#str(unicorn_TF)
############################## #
pdf(file='output/comparison.pdf')
par(mfrow=c(3,2))
## Site-based stuff ##
# Alpha
#head(unicorn_TF$stat)
alphaDF <- data.frame(alpha = output$frescalo.out$alpha, Location = output$frescalo.out$location)
alphaCompare <- merge(unicorn_TF$stat, alphaDF, by = c("Location"))
head(alphaCompare)
alphaCompare$alpha[alphaCompare$alpha > 999.99] <- 999.99
cor(alphaCompare$alpha, alphaCompare$Alpha, use = 'na.or.complete') 
plot(alphaCompare$alpha, alphaCompare$Alpha, main = "Alpha = estimated sampling bias", xlab='R frescalo', ylab='frescalo.exe') # 0.9999995
abline(a = 0, b = 1)

# Estimated species richness
# useful lists etc.
uniSpp <- unique(s$species) # unique species list
sppDF <- data.frame(spIndex = 1:length(uniSpp), species = uniSpp, fLevel = as.numeric(uniSpp))
datM <- merge(s, sppDF, by = "species")
s$datIndex <- 1:nrow(s)
datM <- datM[order(s$datIndex),]
uniSites <- unique(datM$location) # unique data sites
siteDF <- data.frame(siteIndex = 1:length(uniSites), sites = uniSites)
siteNWgts <- wgts[wgts$V1 %in% uniSites,] # just keep relevant neighbourhoods
spnumCompare <- merge(unicorn_TF$stat, unicorns_fs$stat, by.x = c("Location"), by.y=c('location'))
head(spnumCompare)
cor(spnumCompare$Spnum_out, spnumCompare$spnum_out, use = 'na') # 0.99999857
plot(spnumCompare$Spnum_out, spnumCompare$spnum_out, main = "Predicted \n site richness", xlab='R frescalo', ylab='frescalo.exe')
abline(a = 0, b = 1)

## Species x site based stuff ##
# Local weighted frequencies
head(unicorn_TF$freq)
head(unicorns_fs$freq)
freqCompare <- merge(unicorn_TF$freq, unicorns_fs$freq, by.x = c("Location", 'Species'), by.y=c('location','species'))
head(freqCompare)
cor(freqCompare$Freq, freqCompare$freq) # 0.9999999
plot(freqCompare$freq, freqCompare$Freq, main = "Raw species' weighted \n freqs", xlab='R frescalo', ylab='frescalo.exe') # 0.998
abline(a = 0, b = 1)

# Rescaled rank
cor(freqCompare$Rank1, freqCompare$rank_1) # 0.9979
plot(freqCompare$rank_1, freqCompare$Rank1, main = "Rescaled species \nranks (R_ij)", xlab='R frescalo', ylab='frescalo.exe')
abline(a = 0, b = 1)

# Rescaled freq
head(freqCompare)
cor(freqCompare$freq_1, freqCompare$Freq1) # 1
plot(freqCompare$freq_1, freqCompare$Freq1, main = "Rescaled species' \n weighted frequencies (f_ij)", xlab='R frescalo', ylab='frescalo.exe')
abline(a = 0, b = 1)

## Species x time stuff ##
# Time factor [t,j]
unicorn_TF$trend$Time <- sub('1984.5', 1, unicorn_TF$trend$Time)
unicorn_TF$trend$Time <- sub('1994.5', 2, unicorn_TF$trend$Time)
table(unicorn_TF$trend$Time)
trendCompare <- merge(unicorn_TF$trend, unicorns_fs$trend, by.x = c("Time", "Species"), by.y = c("time", "species"))
head(trendCompare)
cor(trendCompare$TFactor, trendCompare$tFactor) # 0.99888974
plot(trendCompare$tFactor, trendCompare$TFactor, main = "Time factors", xlab='R frescalo', ylab='frescalo.exe')
abline(a = 0, b = 1)

# Estimated species total (across sites) after rescaling
# cor(trendCompare$Xest, trendCompare$xest) # # StDev  X  Xspt  Xest N.0.00 N.0.98 are still missing
# plot(trendCompare$value, trendCompare$Xest, main = "Est. per species total \nacross neighbourhoods \n(Sigma_i P_ijt)", xlab='R frescalo', ylab='frescalo.exe')
# abline(a = 0, b = 1)
dev.off()

################################################################ #
## Show some maps ####
source('scripts/os2eastnorth.R')
# 
en=os2eastnorth(GR=output$frescalo.out$location, hectad=T)$en
output$frescalo.out$x = en[,1]
output$frescalo.out$y = en[,2]
# 
# library(ggplot2)
# ggplot(data=output$frescalo.out, aes(x=x, y=y, colour=alpha)) + geom_point()



