# library(Hmisc)

## Ortsangaben
#head(table(Data$Aufnahme.Referenznummer), 10)
Data$Aufnahme.Referenznummer <- gsub('//', '/', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- as.character(Data$Aufnahme.Referenznummer)
sp <- strsplit(Data$Aufnahme.Referenznummer, '/', fixed = TRUE)
#table(sapply(sapply(sp, '[', 1), nchar), useNA = 'ifany')
# 3       4       5       6       7       9      12    <NA>
# 1 2835989      47     357       1       2       2    1684 
#sum(Data$Aufnahme.Referenznummer =='nein')
# 8

Data$Aufnahme.Referenznummer <- gsub(' (QQQ)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub('(QQQ)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(' QQQ)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(' QQQ', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub('QQQ)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub('(QQQ', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(' (QYX)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(' (QQ)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(' (qqq)', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub(')', '', Data$Aufnahme.Referenznummer, fixed = TRUE)
Data$Aufnahme.Referenznummer <- gsub('(', '', Data$Aufnahme.Referenznummer, fixed = TRUE)

sp.1 <- sapply(sp, '[', 1)
table(Data$Aufnahme.Referenznummer[nchar(sp.1) != 4])
# ^7021/4    632/2(QQQ)       6416(42  64177/3(QQQ)       6524f/4  67213/1(QQQ)  67214/1(QQQ)  67214/3(QQQ)       7123f/4 
#       2             1             1             1            21             3             1             1             6 
# 73161/1 75120/14(QQQ)  8119134(QQQ)     8223432 1     8223443 1        Bayern 
#      11             1             2             1             1           357 

Data <- Data[nchar(sp.1) == 4, ]

sp <- strsplit(Data$Aufnahme.Referenznummer, '/')
sp.2 <- sapply(sp, '[', 2)
table(sapply(sp.2, nchar), useNA = 'ifany')
head(Data[sapply(sp.2, nchar, USE.NAMES = FALSE) > 3 & !is.na(sapply(sp.2, nchar)), ])

Data$MTBQ <- paste(sapply(sp, '[', 1), substr(sapply(sp, '[', 2), 1, 1), sep='')

Data <- Data[!is.na(sp.2), ]

### Taxonomie
# library(vegdata)
# gsl <- tax('all', d=T)
# euromed <- tax('all', refl = 'EuroMed')
# 
# taxa <- data.frame(orig=unique(Data$Sippe.Wissenschaftlicher.Name))
# taxa$GermanSL.ID <- gsl$TaxonUsageID[match(taxname.abbr(taxa$orig, concept = 'remove'), taxname.abbr(gsl$TaxonName, concept = 'remove'))]
# taxa$EuroMed.ID <- euromed$TaxonUsageID[match(taxa$orig, euromed$TaxonName)]
# taxa$GermanSL.simplID <- gsl$TaxonUsageID[match(taxname.simplify(taxa$orig, hybrid = 'remove'), taxname.simplify(gsl$TaxonName, hybrid = 'remove'))]
# taxa$orig.simpl <- taxname.simplify(taxa$orig, hybrid = 'remove')
# 
# taxa[is.na(taxa$GermanSL.ID) & is.na(taxa$EuroMed.ID) & is.na(taxa$GermanSL.simplMatch),]
# write.csv2(taxa, '../data/BW/missingTaxa.csv', na = '')


## Restrict data to MTBQ's known from Geographical distances
dist_geo <- readRDS("~/Projekte/sMon/Daten/D/dist_geogr_long.rds")
unique(Data$MTBQ[!Data$MTBQ %in% dist_geo[, 1]])
'
 "79127" "7916 " "65623" "82623"
'
Data <- Data[Data$MTBQ %in% dist_geo[,1], ]

Data$Aufnahme.Datum <- as.character(Data$Aufnahme.Datum)
Data$Sippe.Wissenschaftlicher.Name <- as.character(Data$Sippe.Wissenschaftlicher.Name)

right = function(text, num_char) {
  substr(text, nchar(text) - (num_char-1), nchar(text))
}

Data$Year <- sapply(Data$Aufnahme.Datum, function(x) right(x, 4))
library(parsedate)
Data$Date <- parse_date(Data$Aufnahme.Datum, default_tz = '')

Data <- Data[!is.na(Data$Year), ]
Data <- Data[Data$Year > 1930, ]
Data <- Data[Data$Year != '78 -',]

gsl <- tax('all')
Data$TaxonUsageID <- gsl$TaxonUsageID[match(taxname.simplify(Data$Sippe.Wissenschaftlicher.Name), taxname.simplify(gsl$TaxonName))]
Data$TaxonName <- gsl$TaxonName[match(Data$TaxonUsageID, gsl$TaxonUsageID)] 

save(Data, file = '~/Projekte/sMon/Daten/Bundesländer/BW/Jansen/SippeQuadrDatumSchlüsselwert.Rdata')

