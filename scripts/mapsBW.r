library(rgdal)
bl <- readOGR('~/Projekte/GIS/Shapes Germany/bundeslaender.shp')

wgts <- readRDS('/home/jansen/aSparkleShare/frescalo/data/BW/similarity_weights_100_of_200-BW.rds')
str(wgts)
wgts$target <- as.integer(as.character(wgts$target))
wgts$neighbour <- as.integer(as.character(wgts$neighbour))
source('../scripts/mtb2northeast.r')
mtbqs <- unique(unique(wgts$target), unique(wgts$neighbour))
mtbq <- cbind(mtbqs, matrix(mtb2eastnorth(mtbqs, 'center'), ncol=2))

target <- 74242
png('../data/images/BW-neighbourhood-example.png', width=800, height=1000)
par(mar=c(0,0,0,0))
plot(bl, xlim=c(7.45,10.6), ylim=c(47.6,49.6))
points(mtbq[,2:3], pch=0, cex=3)
nb <- matrix(mtb2eastnorth(wgts$neighbour[wgts$target == target], 'center'), ncol=2)
nb <- cbind(nb, wgts$weight[wgts$target == target])
myColours = c("darkgreen")
## Add an alpha value to a colour
add.alpha <- function(col, alpha=1){
  apply(sapply(col, col2rgb)/255, 2, 
        function(x) rgb(x[1], x[2], x[3], alpha=alpha))  
}
myColoursAlpha <- add.alpha(myColours, alpha=nb[,3])

points(nb[,1:2], pch=22, col= 'darkgreen', cex=3, bg=myColoursAlpha)
points(matrix(mtb2eastnorth(target, 'center'), ncol=2), pch=15, col=2, cex=3)
dev.off()
