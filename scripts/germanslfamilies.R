
library(vegdata)
# We need to download the newest GermanSL
options(tv_home = file.path(path.package('vegdata'), 'tvdata'))
tfile <- tempfile()
download.file('https://germansl.infinitenature.org/GermanSL/1.5/GermanSL.zip', tfile)
unzip(tfile, exdir= file.path(tv.home(), 'Species'))
gsl <- read.dbf(file.path(tv.home(), 'Species', 'GermanSL 1.5', 'species.dbf'), as.is=TRUE)
names(gsl) <- TCS.replace(names(gsl))
vegdata:::store('GermanSl 1.5.detailed', gsl)
# gsl <- tax('all', d=T, syn = FALSE)
gsl <- gsl[gsl$TaxonRank %in% c('VAR', 'SSP', 'SPE', 'AGG', 'SSE', 'SER', 'SEC') & 
             gsl$GRUPPE %in% c('P', 'S'), ]
for(i in 1:nrow(gsl)) gsl$Family[i] <- parent(gsl$TaxonUsageID[i], 
                                              rank = 'FAM', quiet = TRUE)$TaxonUsageID
saveRDS(gsl, 'data/germanslFamilies.Rds')


