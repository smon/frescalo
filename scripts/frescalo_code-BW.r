# 1. check, harmonize and filter primary data
# 2. define time periods, see "timePeriods.r"
# 3. run frescalo and trend analysis

my_packages <- c("foreach", "doParallel", "ggplot2")                                    # Specify the packages needed
not_installed <- my_packages[!(my_packages %in% installed.packages()[ , "Package"])]    # Extract not installed packages
if(length(not_installed)) install.packages(not_installed)                               # Install not installed packages

require(foreach, quietly=T)
require(doParallel, quietly=T)
cl <- makeCluster(2)
registerDoParallel(cl, cores=NULL)

source('./scripts/frescalo_functions.R')
R_star = 0.2703
trend_analysis = TRUE
Phi = 0.74    # The standardisation for recorder effort
chunkSize = 5 # Number of hectads to pass to each compute node
missing.data <- 1

 s <- orchi[which(is.na(orchi$period)), c(1,6,10)]; names(s) <- c("species", "location", "time")
 str(s)
 wgts <- readRDS('../data/BW/similarity_weights_100_of_200-BW.rds')
 names(wgts) <- c('location1','location2','w') # ,'w1','w2','nsim','ndist')

##############################################################
 spLocations = unique(s$location)
 speciesNames = as.character(unique(s$species))   # Create list of unique species
# The variable species could be made numerical to make it more efficient

# For each region record presence/absence of each species (a_ij in Hill 2011)
 locationGroups = as.factor(rep(c(1:ceiling(length(spLocations)/chunkSize)), each=chunkSize))
 sSplit = split(s, locationGroups[match(s$location, spLocations)])  # Split species data up into hectads

idx = iter(sSplit)
speciesList <- foreach(spList = idx, .inorder=T, .combine='c') %dopar% {
  speciesListFun(spList, speciesNames)
}
# Add an additional species list where everything is absent 
speciesList[[length(speciesList)+1]] = rep(0, times=length(speciesNames))
spLocations = c(spLocations,'null_location')

#################################################################
# For each focal regional calculate the sampling effort multipler
dSub = wgts[,1:3]
names(dSub) <- c('location1', 'location2', 'w')
location1List = as.character(unique(dSub$location1))    # Create list of unique focal regions
location1Groups = as.factor(rep(c(1:ceiling(length(location1List)/chunkSize)),each=chunkSize))

dSplit = split(dSub, location1Groups[match(dSub$location1, location1List)])  # Split data up into focal regions
idx2 = iter(dSplit)
output <- foreach(focalData = idx2, .inorder=T, .combine='cfun', .multicombine=TRUE) %dopar% {
  frescalo(focalData, speciesList, spLocations, speciesNames, missing.data=1)
}

saveRDS(output, '../data/BW/output.Rds')
output <- readRDS('../data/BW/output.Rds')
################################################################ #
# Trend analysis  ####
if (trend_analysis) {
  # Do the Frescalo trend analysis if there are more than 1 year bins (use same location groups as sSplit)
  sSplit2 = split(s, as.factor(s$time))  # Split species data up into periods
  idx3 = iter(sSplit2)
  trend.out <- foreach(s_data=idx3, .inorder=T, .combine='rbind') %dopar% {
    trend(s_data, output$freq.out)
  }
}

# for(i in 1:length(sSplit)) {
#  s_data <- sSplit2[[2]]
#  trend(s_data, output$freq.out)
# }

bw_fs <- list()
bw_fs$output <- output
bw_fs$trend <- trend.out

saveRDS(bw_fs, '../data/BW/bw_fs.Rds')

tail(trend.out)
table(trend.out$time)
table(trend.out$species)

for(i in speciesNames) {
  png(paste('../output/', i, '.png', sep=''))
  ind <- trend.out$species == i
  plot(trend.out$tFactor[ind] ~ trend.out$time[ind], type='b')
  dev.off()
}


epi <- orchi[orchi$Sippe.Wissenschaftlicher.Name == 'Epipactis palustris',]
hist(as.numeric(epi$Year))

library(mgcv)
trend.gam <- gam(tFactor ~ time, data= trend.out[ind, ])
plot(trend.gam, se = TRUE, all.terms = TRUE)

################################################################ #
stopCluster(cl)

################################################################ #
## Show some maps ####
# source('scripts/os2eastnorth.R')
# # 
# en=os2eastnorth(GR=output$frescalo.out$location, hectad=T)$en
# output$frescalo.out$x = en[,1]
# output$frescalo.out$y = en[,2]
# # 
# library(ggplot2)
# ggplot(data=output$frescalo.out, aes(x=x, y=y, colour=alpha)) + geom_point()



