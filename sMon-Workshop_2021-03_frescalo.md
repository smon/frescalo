========================================================
author: Florian Jansen
date: 25 März, 2021
width: 1600
height: 1024

# Häufigkeitstrends mit FreScaLo
## sMon Workshop 2021-03-27

Daten, Code und Folien unter
https://git.loe.auf.uni-rostock.de/smon/frescalo/-/archive/master/frescalo-master.zip

[//]: # (font-import: http://fonts.googleapis.com/css?family=Risque font-family: 'Risque' css: custom.css)


FreScaLo
========================================================
 **SCaling of LOcal FREquencies for variation in recorder efforts**

Analyse von Art-Beobachtungsdaten wenn die Beobachtungsintensität variiert

häufig zu lesen: "when recording effort is not known"

we will see ...

[//]: # (For more details on authoring R presentations please visit <https://support.rstudio.com/hc/en-us/articles/200486468>.)

[//]: # (This will show a verbatim inline R expression `` `r 1+1` `` in the output.)


Recorder bias
==================================
left: 40%

1) Auffindbarkeit:

ist methodenabhängig

![](data/images/Muscardinus_Nest_Tube.jpg)

Bsp.: Haselmaus
***
2) Beobachtungsaufwand = Kartierungsintensität = recorder effort:

ist vom Beobachter abhängig

![](data/images/visits.png)

Bsp: **Bias in Butterfly Distribution Maps: The Influence of Hot Spots and Recorder's Home Range**
Dennis et al., Journal of Insect Conservation 4, 73–77 (2000)


Wahrscheinlichkeit der Beobachtung
====================================
left: 60%

*"The methods advocated here depend on the outcome of recording,
not the input of effort. (Hill 2012)"*

![](data/images/detectionVsOccup.png)

*36 Vogelarten in Südost-Spanien* Zamora (2021)
***
<img alt="Detection Rate" src="data/images/factorsDetectionRate.jpg" heigth="900" />


Beobachtungswahrscheinlichkeit steigt mit dem Aufwand
====================================
aber nicht linear

![plot of chunk unnamed-chunk-1](sMon-Workshop_2021-03_frescalo-figure/unnamed-chunk-1-1.png)![plot of chunk unnamed-chunk-1](sMon-Workshop_2021-03_frescalo-figure/unnamed-chunk-1-2.png)


mögliche Korrekturen gegen recorder bias
==================================
- Filtern (Die Guten ins Töpfchen, die Schlechten ins Kröpfchen)
- Telfer: Häufigkeit der Art im Verhältnis zur Gesamtzahl an Beobachtungen
- Anteil häufiger Arten (benchmark species);

aber: 
- räumlich geklumpte Vorkommen, 
- Häufigkeitsunterschiede in der räumlichen Verteilung, 
- lange Rasterkartierungszeiträume 

werden ignoriert, bei Verwendung globaler benchmark species

**FreScaLo:**

regionaler Artenpool (z.B. 100 Gridzellen) und regionale Benchmark Arten = 

=> fehlende Daten in einzelnen Perioden und Gridzellen können ausgeglichen werden


Benchmark Arten
=================================

- häufige Arten, in der Grundeinstellung einfach die häufigsten Arten
- sollten selbst zeitlich möglichst stabil sein
- es wird angenommen, dass ihre Verbreitung räumlich zufällig ist

=>  für FreScaLo sind diese Annahmen auf die jeweiligen Nachbarschaften beschränkt 



Nachbarschaften
===================
*the procedure is to take first the 200 spatially closest hectads and then to select the 100 floristically most similar (Sörensen index) hectads (or regarding abiotic factors) from among those 200*

$w_{i} = (1- \frac{(k-1)^2}{200^2})^4 (1-\frac{(l-1)^2}{100^2})^4$

k = geographical distance

l = floristic similarity (Gefäßpflanzen für die Modellierung der Moose)

<img src="data/images/neighbourhoods-GB.png" alt="GB" width="400"/>

***
    100 ähnlichste Gridzellen bezüglich geographischer Nähe und Ähnlichkeit der abiotischer Eigenschaften (Boden, Klima, Relief, ...)

<img src="data/images/BW-neighbourhood-example.png" alt="neighbours" width="600"/>





regionalisierte Korrektur der Kartierungsintensität
=======================================================
1. Definiere die Nachbarschaft von location i als gewichtete Nachbarzellen, basierend auf räumlicher Nähe und inhaltlicher Ähnlichkeit.

2. Basierend auf realen Artbeobachtungen, kalkuliere die gewichtete lokale Arthäufigkeit $f_{ij}$ für alle Arten in der Nachbarschaft mit Hilfe der Wichte aus Schritt 1

3. Kalkuliere die Nachbarschaftshäufigkeit als frequenzgewichtete lokale Häufigkeit:

 $\sum_{j} f_{ij}^2 ⁄ \sum_{j} f_{ij}$

4. Korrigiere die lokale Artenhäufigkeit mit Hilfe des ‘Beobachtungsmultiplikators’ $\alpha_i$, 
unter der Annahme einer Poisson-Verteilung

When a neighbourhood and its weights have been defined, then $f_{ij}$, the observed frequency of species j in the neighbourhood of hectad i, is defined as

$f_{i\prime} = \sum_{i\prime} {w_{ii\prime}a_{i\prime j}} / \sum_{i\prime} w_{ii\prime}$

where the summation is taken over the neighbourhood, and 

$a_{i\prime j} = 1$ if species j is recorded in hectad $i\prime$ 

and $a_{i\prime j} = 0$ otherwise


Ränge statt absolute Häufigkeiten
==================================

![](data/images/rankfrequenciesBryo.png)
***
![](data/images/adjustedRankfrequenciesBryo.png)

damit können schlecht kartierte Nachbarschaften und gut kartierte miteinander weiter benutzt werden, um z.B. Artentrends zu berechnen.

FreScaLo
=================================
**Anwendung**

1. Abschätzung der Beobachtungsintensität (pro Gridzelle und pro Zeitfenster)

1. unerwartete Vorkommen (Hotspots) oder Abwesenheiten (Kartierlücken einzelner Arten) 

1. Trends: zeitliche Änderung der Arthäufigkeit 


**Voraussetzungen**

1) Grid-basierte Vorkommensdaten (Taxon x Gridzelle x Jahr/Periode)


```
                 Taxon   MTBQ Jahr
1           Abies alba 7100/1 1969
2           Abies alba 7100/2 1969
3 Achillea millefolium 7100/1 1969
```

2) Daten zur Definition der Nachbarschaften:

- abiotische Daten z.B. Klima, Boden; 
- (unabhängige) biologische Daten, etwa Artengemeinschaften anderer Artengruppen (Hill 2012)
- Kartierräume/-methoden


Vorgehen für die Trendanalyse
================================

1. prüfe, harmonisiere und filtere die Primärdaten
2. Definiere die Zeitschnitte
1. Berechne die Nachbarschaften für jede einzelne Gridzelle
1. Berechne die Gewichte
1. Berechne den Korrekturfaktor für die Beobachtungsintensität der betrachteten Zelle pro Periode mit Hilfe von **alpha** (sampling effort multiplier that would equate sampling effort across all regions)

Output:
- frescalo.out contains the recorder-effort multiplier for each focal location, and other related information
- freq.out contains the corrected local frequency of each species in each neighbourhood of a focal region, 


**Berechnung von Trends in der Arthäufigkeit**

The probability that species j is recorded in grid i at time t 

$P_{ijt} = 1 - exp( -Q_{ijt} x_{jt})$

 depends on the recording effort in grid i at time t and on a time-independent probability of species j being found in grid i. For the analysis presented here, a simple model of $Q_{ijt}$ is used, based on the proportion of common (‘benchmark’) species found in grid i and the estimated probability $f_{ij}$ of finding species j there. 
Benchmark species are defined to be those for which
$R^{\prime}_{ij} < R^{*}$
where $R^*$ is a standard value (0.27 = top 27% of species).


Implementierung
==================================
Im Original geschrieben in FORTRAN (https://www.brc.ac.uk/biblio/frescalo-computer-program-analyse-your-biological-records)

Zugriff auf den FORTRAN Code von R aus im Paket sparta (nur Windows)
http://biologicalrecordscentre.github.io/sparta/reference/frescalo.html
Hier auch Dokumentation von Optionen und Abkürzungen für die Ergebnisse

VBA Code von Jan Bijlsma
https://www.blwg.nl/mossen/onderzoek/rapporten/BLWGRapport15.pdf

Native R Rewrite von Jon Yearsley 
https://github.com/DrJonYearsley/frescaloR






Floristische Daten Baden-Württemberg
========================================================
Für eine Einführung in die Geschichte der floristischen Daten B-W siehe Wörz (2015).


```r
# So sehen die Daten aus:

library(Hmisc)
Data <- mdb.get(file.path(rawdatapath, 'SippeQuadrDatumSchlüsselwert.MDB'), charfactor = FALSE)

names(Data)
'"Sippe.Wissenschaftlicher.Name" "Aufnahme.Referenznummer" "Aufnahme.Datum"  "Aufnahmemethode"  "Beobachtung.Schlüsselwert"
'
summary(Data)
'   Sippe.Wissenschaftlicher.Name  
 Epipactis helleborine   :  23365  
 Cephalanthera damasonium:  19567  
 Neottia nidus-avis      :  17748  
 Listera ovata           :  15227  
 Platanthera bifolia     :  13489  
 Urtica dioica           :  10726 
 (Other)                 :2737961
'
```

Ortsangaben = (Teile von) Messtischblätter(n)
========================================================

```r
'
Aufnahme.Referenznummer  
   7221/4 (QQQ):  16988   
   7221/2 (QQQ):  16536  
   7222/3 (QQQ):  15255  
   7122/1 (QQQ):   8868  
   7221/3 (QQQ):   7878   
   7220/2 (QQQ):   7359   
   (Other)     :2765199
'
```


```r
head(names(table(Data$Aufnahme.Referenznummer)), 10)
' ""   "^7021/4 (QQQ)" "6017/14 (QQQ)" "6018/1 (QQQ)"  "6018/2 (QQQ)"  "6018/23 (QQQ)" "6018/24 (QQQ)" "6018/33 (QQQ)" "6018/34 (QQQ)"
'

# Zahl der Ziffern vor dem "/"
sp <- strsplit(Data$Aufnahme.Referenznummer, '/', fixed = TRUE)
table(sapply(sapply(sp, '[', 1), nchar), useNA = 'ifany')
# 3       4       5       6       7       9      12    <NA>
# 1 2835989      47     357       1       2       2    1684 
```


Datum
========================================================

```r
'    Aufnahme.Datum
1970 - 1993:  44133
2010 - 2016:  39642
1991 - 2004:  37736
1970 - 1985:  31801
1970       :  28884
1975 - 1985:  27262
(Other)    :2628625
'
```

Methoden
========================================================


```r
'
                Aufnahmemethode  Beobachtung.Schlüsselwert
   Florist. Kartierung :2615987   BFN0023400000049:      1    
   Karteiauswertung    :  79091   BFN002340000004A:      1    
   Literaturauswertung :  72278   BFN002340000004B:      1    
   Herbarauswertung    :  50108   BFN002340000004C:      1    
   FlorKart Einzelnotiz:   9636   BFN002340000004D:      1    
   FlorKart Punktdaten :   6070   BFN002340000004E:      1    
   (Other)             :   4913   (Other)         :2838077   
'
```

[//]: # (source('./scripts/Data-BW.r')

Subset für den Workshop
===============================
Nach dem ersten Screening and Cleaning bleiben 2.8 Mill. Beobachtungen übrig.
Da wir versprochen haben die Daten aus BW nicht weiterzugeben und auch damit die weiteren Berechnung schneller geht, wählen wir aus diesem Datensatz zufällig 10% aus.

```r
# load('~/Projekte/sMon/Daten/Bundesländer/BW/Jansen/SippeQuadrDatumSchlüsselwert.Rdata')
set.seed(123)
# Data <- Data[sample(x = 1:nrow(Data), size = nrow(Data)*0.1), ]
# saveRDS(Data, "./data/BW/SippeQuadrDatumSchlüsselwert.Rds")
```

Ab hier ist nicht nur der Code, sondern auch der Datensatz verfügbar:

git clone https://git.loe.auf.uni-rostock.de/smon/frescalo.git

oder als ZIP-Datei:

https://git.loe.auf.uni-rostock.de/smon/frescalo/-/archive/master/frescalo-master.zip



Explorative Datenanalyse
===============================


```r
Data <- readRDS(file = "./data/BW/SippeQuadrDatumSchlüsselwert.Rds")
tax.freq <- table(Data$Sippe.Wissenschaftlicher.Name)
plot(sort(tax.freq, decreasing = TRUE), ylab='Zahl der Beobachtungen')
```

![Number of observations.](sMon-Workshop_2021-03_frescalo-figure/nrbeo-1.png)



Detectability oder Sich änderndes Interesse
==========================================
Die absolute Beobachtungsintensität darf sich über die Zeit ändern, nicht jedoch die relative. 

Genau das ist aber leider häufig der Fall, insbesondere bei

- taxonomisch neuen Arten: Aufspaltungen von Sippen, neuen Erkenntnissen über das Vorkommen von Arten im Gebiet führen dazu, das Taxa als neu erscheinen, obwohl die betreffenden Populationen schon immer da waren
- explizite Kartierkampagnen bestimmter Arten oder Artengruppen können dazu führen, dass sich das Verhältnis der Artbeobachtungen verändert, z.B durch eine explizite Kartierung von Orchideen oder Küstenarten
- In BW wurde 1969 der Arbeitskreis Heimische Orchideen (\url{https://www.orchids.de}) gegründet und ist seitdem kontinuierlich aktiv in der Beobachtung der Orchideen
- Allerweltsarten sind in Citizen Science Datensätzen in aller Regel unterrepräsentiert, es sei denn, es gibt systematische Kartierprogramme, meist in Form von Rasterkartierungen 

=> Um abzuschätzen, wie groß das Problem ist, schauen wir uns an, wie oft Orchideen und Asteraceen pro Jahr beobachtet wurden (unabhängig von der Häufigkeit der einzelnen Art in diesem Jahr und ihrer räumlichen Verteilung).

Dazu müssen wir aber zuerst den Datensatz taxonomisch harmonisieren.




Taxonomische Harmonisierung
=================================
Wir haben 60 Orchideentaxa im Datensatz. Davon sind aber einige Taxa genestet: Unterart, Art und Aggregat im gleichen Taxonast können wir nicht alle modellieren, denn sie sind nicht unabhängig und meist unregelmäßig in der Zeit kartiert. Die automatische Harmonisierung mit der taxval Funktion aus dem vegdata Package hebt in diesen Fällen alle Beobachtungen auf die jeweils oberste vorkommende Stufe (Aggregat, Section etc.).



```r
library(vegdata)
gsl <- tax('all')
orchi$TaxonUsageID <- gsl$TaxonUsageID[match(orchi$Sippe.Wissenschaftlicher.Name, gsl$TaxonName)]

orchi <- taxval(obs = orchi, refl = 'GermanSL 1.5', maxtaxlevel = 'AGG')
'
Original number of names: 60
13 conflicting child taxa found in dataset. 
1 conflicting child taxa found in dataset. 

Number of taxa after harmonisation: 47
'
orchi$TaxonName <- gsl$TaxonName[match(orchi$TaxonUsageID, gsl$TaxonUsageID)]
```

Die 60 Orchideentaxa werden durch die Harmonisierung zu 47 modellierbaren Taxa zusammengefasst.

Das Gleiche machen wir für die Asteraceen und den Gesamtdatensatz




Zeitabschnitte
======================
FreScaLo und ähnliche Occupancy models sehen vor, dass sich die Kartierungsintensität in Raum und Zeit ändern darf, nicht aber die relative Häufigkeit in der Beobachtung der Arten. 

![plot of chunk TaxaByYear](sMon-Workshop_2021-03_frescalo-figure/TaxaByYear-1.png)

Während die Korbblütler ein guter Repräsentant des Gesamtdatensatzes sind, macht sich bei den Orchideen die Gründung des AHO 1969 bemerkbar. Die Kartierung ist seitdem konstant hoch und zwischen den Rasterkartierungen wesentlich intensiver als bei den anderen Arten.


```r
layout(matrix(1:2, nrow=1))
par(mar=c(3,3,2,.5), cex=2)
orchiObsByPeriod <- table(orchi$Year)
plot(as.numeric(orchiObsByPeriod) ~ as.numeric(names(orchiObsByPeriod)), pch = 19,
     main='Orchideen/Jahr', xlab='', ylab='')
abline(v = 1969, lty = 2, col = 2, lwd = 2)
text(1969, 10, pos = 2, 'Gründung der \n AHO', col = 2)
rect(1970, -10, 1988, 1200, density = 15, border = NULL, lty =2)
rect(2005, -10, 2021, 1200, density = 15, border = NULL, lty =2)

asterObsByPeriod <- table( aster$Year)
plot(as.numeric(asterObsByPeriod) ~ as.numeric(names(asterObsByPeriod)), pch = 19,
     main='Korblütler/Jahr', xlab='', ylab='')
rect(1970, -10, 1988, 2000, density = 15, border = NULL, lty =2)
rect(2005, -10, 2021, 2000, density = 15, border = NULL, lty =2)
```

![plot of chunk unnamed-chunk-8](sMon-Workshop_2021-03_frescalo-figure/unnamed-chunk-8-1.png)


Trends pro Dekade
========================
Prinzipiell sollte die Kartierungsintensität in BW ausreichen, alle Arten ab 1970 und pro Jahrzehnt zu modellieren.

Für unseren auf 10% reduzierten Testdatensatz wird es allerdings knapp. Die Ergebnisse sind mit entsprechender Vorsicht zu genießen.



```

   0    1    2    3    4    5 
 203 1744 3364 6573 5512 1274 
```


Frescalo Neighbourhoods
======================
 **SCaling of LOcal FREquencies for variation in recorder efforts** (FreScaLo)


```r
simattr <- readRDS('./data/similarity_attribs.rds')
names(simattr)
```

```
 [1] "MTB_Q"          "Elev_mean"      "Elev_min"       "Elev_max"      
 [5] "Slope_mean"     "Slope_min"      "Slope_max"      "Roughness_mean"
 [9] "Aspect_mean"    "Aspect_range"   "Auenbo"         "Auenbo.1"      
[13] "B\xf6den."      "Braune"         "Braune.1"       "Braune.2"      
[17] "Braune.3"       "Braune.4"       "Braune.5"       "Braune.6"      
[21] "Braune.7"       "Braune.8"       "Braune.9"       "Braune.10"     
[25] "Braune.11"      "Braune.12"      "Braune.13"      "Braune.14"     
[29] "Braune.15"      "Braune.16"      "Eisenh"         "Fahler"        
[33] "Gew\xe4ss"      "Gley.d"         "Hochmo"         "Kalkma"        
[37] "Kleima"         "Nieder"         "Parabr"         "Parabr.1"      
[41] "Parabr.2"       "Parabr.3"       "Parabr.4"       "Parabr.5"      
[45] "Parabr.6"       "Parare"         "Parare.1"       "Parare.2"      
[49] "Peloso"         "Podsol"         "Podsol.1"       "Podsol.2"      
[53] "Podsol.3"       "Podsol.4"       "Podsol.5"       "Pseudo"        
[57] "Pseudo.1"       "Pseudo.2"       "Pseudo.3"       "Pseudo.4"      
[61] "Regoso"         "Rendzi"         "Rendzi.1"       "Rohb\xf6d"     
[65] "Techno"         "Tscher"         "Tscher.1"       "Tscher.2"      
[69] "Tscher.3"       "Versie"         "Wattbo"         "tempmin_mean"  
[73] "tempmean_mean"  "tempmax_mean"   "precip_mean"   
```

Zur Erklärung der Attribute siehe https://data.botanik.uni-halle.de/smon/datasets/54


```r
# source('./scripts/Creating_similarity_weights.R')
```

Nachbarschaften
========================
![target](data/images/BW-neighbourhood-example.png)



FRESCALO und Trendberechnung
===================================
vermuteter Recorder Bias pro Gridzelle x Periode und daraus Berechnung der Wahrscheinlichkeit des Vorkommens einer Art pro Gridzelle und Periode


```r
source('./scripts/frescalo-BW.r')
```


```
  location            species pres      freq    freq_1 rank     rank_1 benchmark
1    71231      Listera ovata    1 0.9737235 0.9999944    1 0.06132922         1
2    71231 Neottia nidus-avis    1 0.9477861 0.9999452    2 0.12265844         1
```

*	Location: Name of location
*	Species:	Name of species
*	Pres:	Record of species in location (1 = recorded, 0 = not recorded)
*	Freq:	Frequency of species in neighbourhood of location
*	Freq_1:	Estimated probability of occurrence, i.e. frequency of species after rescaling
*	Rank:	Rank of frequency in neighbourhood of location
*	Rank_1:	Rescaled rank, defined as Rank/Estimated species richness
* benchmark: marked as benchmark species

https://rdrr.io/github/BiologicalRecordsCentre/sparta/man/frescalo.html


tFactor (relative Häufigkeit in der Zeit)
==================

```
                   species time   tFactor
1        Ophrys holoserica    1 0.9431127
2       Ophrys insectifera    1 0.3608827
3    Cypripedium calceolus    1 0.0000000
4 Cephalanthera damasonium    1 1.0081516
5            Listera ovata    1 0.4478812
6       Neottia nidus-avis    1 4.8859986
```

```
                   species time   tFactor
37      Ophrys insectifera    2 0.6479802
38       Ophrys holoserica    2 0.2843872
39 Himantoglossum hircinum    2 0.0000000
40           Listera ovata    2 0.9910061
41        Orchis militaris    2 0.4622993
42 Gymnadenia odoratissima    2 0.2601306
```

* Species:	Taxonname
* Time:	Time period (e.g. "1970-1980", character string)
* tFactor:	Time factor, geschätzte relative Häufigkeit der Art pro Periode

Trendplot Netzblatt
=========================
![Goodyera repens](output/Goodyera repens.png)

Netzblatt - Verlierer der Eutrophierung


Trendplot Weißes Waldvöglein
=========================
![Cephalanthera damasonium](output/Cephalanthera damasonium.png)

 - profitiert von (jungen) Fichtenforsten

1990er Pike bei praktisch allen Orchideen zu sehen (weil mit den Asteraceen zusammen modelliert!)


Trendplot Achillea millefolium agg.
====================================
![Achillea millefolium agg.](output/Achillea millefolium agg..png)

das korrespondierende Tal!

Trendplot Solidago virgaurea
====================================
![Solidago virgaurea](output/Solidago virgaurea.png)

Trendplot Antennaria dioica
====================================
![Antennaria dioica](output/Antennaria dioica.png)







FreScaLo hilft
==============================

1. gegen ungleichmäßiges Sampling im Raum 
2. gegen ungleichmäßiges Sampling in der Zeit

(aber nur solange es für alle Arten gleich bzw. unsystematisch ungleich ist)

**But we need to correct for changes in recorder behaviour**


$$
\begin{aligned}
\text{Thanks for listening, now we get our hand on the data.}
\end{aligned}
$$
